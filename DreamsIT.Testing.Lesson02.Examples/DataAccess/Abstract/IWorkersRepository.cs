using System.Collections.Generic;
using System.Linq;
using DreamsIT.Testing.Lesson02.Examples.Models;

namespace DreamsIT.Testing.Lesson02.Examples.DataAccess.Abstract
{
    public interface IWorkersRepository
    {
        IQueryable<Worker> WorkersList { get; }
        void SaveWorker(Worker worker);
        void RemoveWorker(long id);
    }
}