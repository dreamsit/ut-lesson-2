﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Testing.Lesson02.Examples.DataAccess.Abstract;
using DreamsIT.Testing.Lesson02.Examples.Models;
using Newtonsoft.Json;

namespace DreamsIT.Testing.Lesson02.Examples.DataAccess.Concrets
{
    public class JsonWorkersRepository : IWorkersRepository
    {
        private readonly string _filepath;
        private List<Worker> _workers;

        public JsonWorkersRepository(string filepath)
        {
            _filepath = filepath;
        }

        public IQueryable<Worker> WorkersList
        {
            get { return (_workers ?? (_workers = getWorkers())).AsQueryable(); }
        }

        public void SaveWorker(Worker worker)
        {
            JsonSerializer serializer=new JsonSerializer();
            if (worker.Id == 0)
            {
                worker.Id = DateTime.UtcNow.Ticks;
                var stringBuilder = new StringBuilder();
                serializer.Serialize(new StringWriter(stringBuilder), worker);
                appendToFile(stringBuilder.ToString());
            }
            else
            {
                deleteWorkerFromList(worker.Id);
                _workers.Add(worker);
                storeList();
            }
        }

        public void RemoveWorker(long id)
        {
            deleteWorkerFromList(id);
            storeList();
        }

        private void deleteWorkerFromList(long id)
        {
            var existed = WorkersList.First(x => x.Id == id);
            _workers.Remove(existed);
        }


        private void appendToFile(string content)
        {
            checkFile();
            File.AppendAllText(_filepath, content);
            _workers = null;
        }

        private void storeList()
        {
            File.Delete(_filepath);
            checkFile();
            var content = new StringBuilder();
            JsonSerializer serializer=new JsonSerializer();
            serializer.Serialize(new StringWriter(content), _workers);
            appendToFile(content.ToString());
            _workers = null;
        }

        private void checkFile()
        {
            if (!File.Exists(_filepath))
            {
                var stream = File.CreateText(_filepath);
                stream.Close();
                stream.Dispose();
            }
        }

        private List<Worker> getWorkers()
        {
            if (File.Exists(_filepath))
            {
                var content = File.ReadAllText(_filepath);

                JsonSerializer serializer = new JsonSerializer();
                return serializer.Deserialize<List<Worker>>(new JsonTextReader(new StringReader(content)));
            }
            return new List<Worker>();
        }
    }
}
