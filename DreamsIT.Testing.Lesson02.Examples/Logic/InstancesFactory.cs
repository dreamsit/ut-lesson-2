﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Testing.Lesson02.Examples.DataAccess.Abstract;
using DreamsIT.Testing.Lesson02.Examples.DataAccess.Concrets;

namespace DreamsIT.Testing.Lesson02.Examples.Logic
{
    public class InstancesFactory
    {
        private static IWorkersRepository _workersRepository;

        public static IWorkersRepository GetWorkersRepository()
        {
            return _workersRepository ?? new JsonWorkersRepository("workers.json");
        }

        public static void SetRepository(IWorkersRepository repository)
        {
            _workersRepository = repository;
        }
    }
}
