﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Testing.Lesson02.Examples.Logic
{
    public class StaticLogicHelper
    {
        public static int GetCurrentMonthWorkDays()
        {
            return 20;
        }

        public static int GetWorkDayLenght()
        {
            return 8;
        }
    }
}
