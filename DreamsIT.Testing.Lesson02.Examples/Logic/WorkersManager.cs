﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Testing.Lesson02.Examples.DataAccess.Abstract;
using DreamsIT.Testing.Lesson02.Examples.DataAccess.Concrets;
using DreamsIT.Testing.Lesson02.Examples.Models;

namespace DreamsIT.Testing.Lesson02.Examples.Logic
{
    public class WorkersManager
    {
        public WorkersManager()
        {
            _repository = InstancesFactory.GetWorkersRepository();
        }

        public WorkersManager(IWorkersRepository repository)
        {
            _repository = repository;
        }

        protected IWorkersRepository _repository;

        public IWorkersRepository WorkersRepository
        {
            get { return _repository; }
            set { _repository = value; }
        }

        public virtual Worker HireWorker(string fullName, string department, decimal salary)
        {
            if (salary < 0)
            {
                throw new ArgumentException("Salary must be equal or greater than 0");
            }

            Worker worker=new Worker()
            {
                FullName = fullName,
                Department = department,
                SalaryPerHour = salary,
                HireTime = DateTime.UtcNow
            };

            _repository.SaveWorker(worker);
            return worker;
        }

        public virtual Worker HireWorker(string fullName, string department, decimal salary, List<Worker> workers)
        {
            if (salary < 0)
            {
                throw new ArgumentException("Salary must be equal or greater than 0");
            }

            Worker worker = new Worker()
            {
                FullName = fullName,
                Department = department,
                SalaryPerHour = salary,
                HireTime = DateTime.UtcNow
            };

            workers.Add(worker);

            return worker;
        }



        public IEnumerable<Worker> FindWorkers(string fullName = null, string department = null, decimal salaryFrom = -1,
            decimal salaryTo = -1)
        {
            IEnumerable<Worker> result = _repository.WorkersList;

            if (!String.IsNullOrEmpty(fullName))
            {
                result = result.Where(x => x.FullName.Contains(fullName));
            }
            if (!String.IsNullOrEmpty(department))
            {
                result = result.Where(x => x.Department == department);
            }
            if (salaryFrom >= 0)
            {
                result = result.Where(x => x.SalaryPerHour >= salaryFrom);
            }
            if (salaryTo >= 0)
            {
                result = result.Where(x => x.SalaryPerHour <= salaryTo);
            }

            return result;
        }

        public void FireWorker(long id)
        {
            var worker = _repository.WorkersList.FirstOrDefault(x => x.Id == id);
            if(worker==null)
                throw new Exception("Worker not found");

            if(worker.FireTime.HasValue)
                throw new Exception("Worker already fired");

            worker.FireTime = DateTime.UtcNow;
            _repository.SaveWorker(worker);
        }

        public IEnumerable<Worker> FindWorkersInDepartment(string department)
        {
            return _repository.WorkersList.Where(x => x.Department == department);
        }

        public IEnumerable<Worker> AllWorkers
        {
            get { return _repository.WorkersList; }
        }
    }
}
