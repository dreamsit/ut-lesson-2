﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Testing.Lesson02.Examples.Models;

namespace DreamsIT.Testing.Lesson02.Examples.Logic
{
    public class WorkersSalaryManager
    {
        public decimal CalculateWorkerSalaryPerMonth(Worker worker)
        {
            var workDaysInMonth = StaticLogicHelper.GetCurrentMonthWorkDays();
            var workDayLength = StaticLogicHelper.GetWorkDayLenght();
            return worker.SalaryPerHour * workDayLength * workDaysInMonth;
        }
    }
}
