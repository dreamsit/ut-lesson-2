﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Testing.Lesson02.Examples.Models
{
    public class Salary
    {
        public DateTime ChargePeriodStart { get; set; }
        public DateTime ChargePeriodEnd { get; set; }
        public Worker Worker { get; set; }
        public decimal PayedSum { get; set; }
        public decimal SumToPay { get; set; }
    }
}
