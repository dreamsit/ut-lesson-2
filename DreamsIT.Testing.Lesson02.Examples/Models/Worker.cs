﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DreamsIT.Testing.Lesson02.Examples.Models
{
    public class Worker
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string Department { get; set; }
        public decimal SalaryPerHour { get; set; }
        public DateTime HireTime { get; set; }
        public DateTime? FireTime { get; set; }
    }
}
