﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Testing.Lesson02.Examples.Logic;
using DreamsIT.Testing.Lesson02.Examples.Models;
using DreamsIT.Testing.Lesson03.FakeData.Fakes;
using NUnit.Framework;

namespace DreamsIT.Testing.Lesson02.ExamplesTests
{
    [TestFixture]
    public class WorkersManagerTests
    {
        private FakeWorkersRepository _repository;
        private string _fullName;
        private string _department;
        private decimal _salary;

        [SetUp]
        public void SetUp()
        {
            _repository = new FakeWorkersRepository();
            _fullName = "Ivanov Ivan";
            _department = "Management";
            _salary = 10.5M;
        }

        [Test]
        public void HireWorker_WithAllRequiredFields_AddWorkerToList()
        {
            var manager = new WorkersManager(_repository);

            Worker worker = manager.HireWorker(_fullName, _department, _salary);
            var existed = _repository.WorkersList.FirstOrDefault(w => w.Id == worker.Id);

            Assert.IsNotNull(worker, "Returned worker is null");
            Assert.IsNotNull(existed, "Worker not added to list");
        }

        [Test]
        public void HireWorker_WithAllRequiredFields_ReturnsWorkerWithSameData()
        {
            var manager = new WorkersManager(null);
            manager.WorkersRepository = _repository;

            Worker worker = manager.HireWorker(_fullName, _department, _salary);

            Assert.AreEqual(_fullName, worker.FullName, "Worker full name is different");
            Assert.AreEqual(_department, worker.Department, "Worker department is different");
            Assert.AreEqual(_salary, worker.SalaryPerHour, "Worker salary is different");
        }

        [Test]
        public void HireWorker_WithAllRequiredFields_ReturnsWorkerSameAsInList()
        {
            InstancesFactory.SetRepository(_repository);
            var manager = new WorkersManager();

            Worker worker = manager.HireWorker(_fullName, _department, _salary);
            var existed = InstancesFactory.GetWorkersRepository().WorkersList.FirstOrDefault(w => w.Id == worker.Id);

            Assert.AreEqual(worker.FullName, existed.FullName, "Workers full name is different");
            Assert.AreEqual(worker.Department, existed.Department, "Workers department is different");
            Assert.AreEqual(worker.SalaryPerHour, existed.SalaryPerHour, "Workers salary is different");
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void HireWorker_WithSalaryLessThanZero_TrowsException()
        {
            var manager = new WorkersManagerWrap();

            manager.HireWorker(_fullName, _department, -1);
        }
    }
}
