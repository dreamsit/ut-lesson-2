﻿using System.Collections.Generic;
using System.Linq;
using DreamsIT.Testing.Lesson02.Examples.DataAccess.Abstract;
using DreamsIT.Testing.Lesson02.Examples.Models;

namespace DreamsIT.Testing.Lesson03.FakeData.Fakes
{
    public class FakeWorkersRepository:IWorkersRepository
    {
        private readonly List<Worker> _workers; 

        public FakeWorkersRepository()
        {
            _workers=new List<Worker>();
        }

        public IQueryable<Worker> WorkersList
        {
            get { return _workers.AsQueryable(); }
        }

        public void SaveWorker(Worker worker)
        {
            if(worker!=null)
                _workers.Add(worker);
        }

        public void RemoveWorker(long id)
        {
            var existed = _workers.FirstOrDefault(x => x.Id == id);
            if (existed != null)
                _workers.Remove(existed);
        }
    }
}
