﻿using DreamsIT.Testing.Lesson02.Examples.Logic;
using DreamsIT.Testing.Lesson02.Examples.Models;

namespace DreamsIT.Testing.Lesson03.FakeData.Fakes
{
    public class WorkersManagerWrap:WorkersManager
    {
        public override Worker HireWorker(string fullName, string department, decimal salary)
        {
            _repository = new FakeWorkersRepository();
            return base.HireWorker(fullName, department, salary);
        }
    }
}
