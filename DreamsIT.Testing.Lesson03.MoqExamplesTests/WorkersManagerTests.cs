﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Testing.Lesson02.Examples.DataAccess.Abstract;
using DreamsIT.Testing.Lesson02.Examples.Logic;
using DreamsIT.Testing.Lesson02.Examples.Models;
using Moq;
using NUnit.Framework;

namespace DreamsIT.Testing.Lesson03.MoqExamplesTests
{
    [TestFixture]
    public class WorkersManagerTests
    {
        private string _fullName;
        private string _department;
        private decimal _salary;

        [SetUp]
        public void SetUp()
        {
            _fullName = "Ivanov Ivan";
            _department = "Management";
            _salary = 10.5M;
        }

        [Test]
        public void HireWorker_WithAllRequiredFields_AddWorkerToList()
        {
            List<Worker> workers=new List<Worker>();
            
            var mockRepository = new Mock<IWorkersRepository>();
            mockRepository.Setup(r => r.WorkersList).Returns(workers.AsQueryable);
            mockRepository.Setup(r => r.SaveWorker(It.IsAny<Worker>())).Callback<Worker>(workers.Add);

            IWorkersRepository repository = mockRepository.Object;
            var manager = new WorkersManager(repository);

            Worker worker = manager.HireWorker(_fullName, _department, _salary);

            Assert.IsNotNull(worker, "Returned worker is null");
            Assert.IsTrue(manager.AllWorkers.Contains(worker));
        }

        [Test]
        public void HireWorker_WithAllRequiredFields_WorkerWasSaved()
        {
            List<Worker> workers = new List<Worker>();

            var mockRepository = new Mock<IWorkersRepository>();
            mockRepository.Setup(r => r.WorkersList).Returns(workers.AsQueryable);
            mockRepository.Setup(r => r.SaveWorker(It.IsAny<Worker>()));

            IWorkersRepository repository = mockRepository.Object;
            var manager = new WorkersManager(repository);

            Worker worker = manager.HireWorker(_fullName, _department, _salary);

            Assert.IsNotNull(worker, "Returned worker is null");
            mockRepository.Verify(x=>x.SaveWorker(worker), Times.AtLeastOnce);
            Assert.IsTrue(manager.AllWorkers.Contains(worker));
        }
    }
}
