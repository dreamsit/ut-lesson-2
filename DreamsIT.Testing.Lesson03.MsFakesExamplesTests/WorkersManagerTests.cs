﻿using System;
using System.Linq;
using System.Threading;
using DreamsIT.Testing.Lesson02.Examples.Logic;
using DreamsIT.Testing.Lesson02.Examples.Models;
using DreamsIT.Testing.Lesson03.FakeData.Fakes;
using Microsoft.QualityTools.Testing.Fakes;
using NUnit.Framework;

namespace DreamsIT.Testing.Lesson03.MsFakesExamplesTests
{
    [TestFixture]
    public class WorkersManagerTests
    {
        private FakeWorkersRepository _repository;
        private string _fullName;
        private string _department;
        private decimal _salary;

        [SetUp]
        public void SetUp()
        {
            _repository = new FakeWorkersRepository();
            _fullName = "Ivanov Ivan";
            _department = "Management";
            _salary = 10.5M;
        }

        [Test]
        public void HireWorker_WithAllRequiredFields_ReturnsWorkerWithCorrectHireTime_WillFail()
        {
            var manager = new WorkersManager(_repository);
            var time = DateTime.UtcNow;
            Thread.Sleep(100);
            Worker worker = manager.HireWorker(_fullName, _department, _salary);

            Assert.AreEqual(time, worker.HireTime, "Worker hire time is different");
        }

        [Test]
        public void HireWorker_WithAllRequiredFields_ReturnsWorkerWithCorrectHireTime_WillSuccess()
        {
            var manager = new WorkersManager(_repository);
            var fixedTime = DateTime.UtcNow;
            Thread.Sleep(100);
            using (ShimsContext.Create())
            {
                System.Fakes.ShimDateTime.UtcNowGet = () => fixedTime;
                Worker worker = manager.HireWorker(_fullName, _department, _salary);

                Assert.AreEqual(fixedTime, worker.HireTime, "Worker hire time is different");
            }
        }
    }
}
