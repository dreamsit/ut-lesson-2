﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DreamsIT.Testing.Lesson02.Examples.Logic;
using DreamsIT.Testing.Lesson02.Examples.Logic.Fakes;
using DreamsIT.Testing.Lesson02.Examples.Models;
using DreamsIT.Testing.Lesson03.FakeData.Fakes;
using Microsoft.QualityTools.Testing.Fakes;
using NUnit.Framework;

namespace DreamsIT.Testing.Lesson03.MsFakesExamplesTests
{
    [TestFixture]
    public class WorkersSalaryManagerTests
    {
        [Test]
        public void CalculateWorkerSalaryPerMonth_ReturnsCorrectResult()
        {
            var manager = new WorkersSalaryManager();
            var worker = new Worker()
            {
                SalaryPerHour = 20
            };
            var fakeWorkDays = 22;
            var fakeDayLength = 6;
            var expectedSalary = 2640;

            using (ShimsContext.Create())
            {
                ShimStaticLogicHelper.GetCurrentMonthWorkDays = () => fakeWorkDays;
                ShimStaticLogicHelper.GetWorkDayLenght = () => fakeDayLength;

                var calculatedSalary = manager.CalculateWorkerSalaryPerMonth(worker);

                Assert.AreEqual(expectedSalary, calculatedSalary, "Salary is incorrect");
            }

        }
    }
}
